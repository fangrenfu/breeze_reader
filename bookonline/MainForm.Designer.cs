﻿namespace BookOnline
{
    partial class mainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.workStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.readTip = new System.Windows.Forms.ToolStripStatusLabel();
            this.softversion = new System.Windows.Forms.ToolStripStatusLabel();
            this.regVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.onlineStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectType = new System.Windows.Forms.ToolStripStatusLabel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.myBrowser = new System.Windows.Forms.WebBrowser();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.tsbBook = new System.Windows.Forms.ToolStripButton();
            this.tsbRank = new System.Windows.Forms.ToolStripButton();
            this.tsbMyBook = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbNew = new System.Windows.Forms.ToolStripButton();
            this.tsbAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbFullScreen = new System.Windows.Forms.ToolStripButton();
            this.tsbVoice = new System.Windows.Forms.ToolStripButton();
            this.tsbOnLine = new System.Windows.Forms.ToolStripButton();
            this.tsbSelect = new System.Windows.Forms.ToolStripButton();
            this.tsbDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbSave = new System.Windows.Forms.ToolStripButton();
            this.tsbSearch = new System.Windows.Forms.ToolStripButton();
            this.stbUpdate = new System.Windows.Forms.ToolStripButton();
            this.tsbAbout = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsNew = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsDown = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsDel = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // statusStrip
            // 
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workStatus,
            this.readTip,
            this.softversion,
            this.regVersion,
            this.onlineStatus,
            this.selectType});
            this.statusStrip.Location = new System.Drawing.Point(0, 0);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(822, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // workStatus
            // 
            this.workStatus.Name = "workStatus";
            this.workStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.workStatus.Size = new System.Drawing.Size(307, 17);
            this.workStatus.Spring = true;
            this.workStatus.Text = "就绪";
            this.workStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // readTip
            // 
            this.readTip.BackColor = System.Drawing.SystemColors.Control;
            this.readTip.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.readTip.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.readTip.Name = "readTip";
            this.readTip.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.readTip.Size = new System.Drawing.Size(282, 19);
            this.readTip.Text = "小键盘：← 上章  | →下章 | Home 目录 | End 最后";
            // 
            // softversion
            // 
            this.softversion.Name = "softversion";
            this.softversion.Size = new System.Drawing.Size(35, 17);
            this.softversion.Text = "版本:";
            // 
            // regVersion
            // 
            this.regVersion.Name = "regVersion";
            this.regVersion.Size = new System.Drawing.Size(47, 17);
            this.regVersion.Text = "特征库:";
            // 
            // onlineStatus
            // 
            this.onlineStatus.Name = "onlineStatus";
            this.onlineStatus.Size = new System.Drawing.Size(68, 17);
            this.onlineStatus.Text = "状态：在线";
            // 
            // selectType
            // 
            this.selectType.Name = "selectType";
            this.selectType.Size = new System.Drawing.Size(68, 17);
            this.selectType.Text = "模式：过滤";
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.Size = new System.Drawing.Size(692, 573);
            // 
            // myBrowser
            // 
            this.myBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myBrowser.Location = new System.Drawing.Point(0, 0);
            this.myBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.myBrowser.Name = "myBrowser";
            this.myBrowser.ScriptErrorsSuppressed = true;
            this.myBrowser.Size = new System.Drawing.Size(628, 558);
            this.myBrowser.TabIndex = 4;
            this.myBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.myBrowser_DocumentCompleted);
            this.myBrowser.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.myBrowser_Navigating);
            this.myBrowser.NewWindow += new System.ComponentModel.CancelEventHandler(this.myBrowser_NewWindow);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(822, 558);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(822, 605);
            this.toolStripContainer1.TabIndex = 5;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeView);
            this.splitContainer.Panel1MinSize = 0;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.myBrowser);
            this.splitContainer.Size = new System.Drawing.Size(822, 558);
            this.splitContainer.SplitterDistance = 190;
            this.splitContainer.TabIndex = 5;
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageList;
            this.treeView.Indent = 19;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 2;
            this.treeView.Size = new System.Drawing.Size(190, 558);
            this.treeView.TabIndex = 0;
            this.treeView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseUp);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "SearchButton.ico");
            this.imageList.Images.SetKeyName(1, "load");
            this.imageList.Images.SetKeyName(2, "book");
            this.imageList.Images.SetKeyName(3, "type");
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbBook,
            this.tsbRank,
            this.tsbMyBook,
            this.toolStripSeparator1,
            this.tsbNew,
            this.tsbAdd,
            this.toolStripSeparator3,
            this.tsbFullScreen,
            this.tsbVoice,
            this.tsbOnLine,
            this.tsbSelect,
            this.tsbDown,
            this.toolStripSeparator2,
            this.tsbSave,
            this.tsbSearch,
            this.stbUpdate,
            this.tsbAbout});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(819, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // tsbBook
            // 
            this.tsbBook.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbBook.Image = ((System.Drawing.Image)(resources.GetObject("tsbBook.Image")));
            this.tsbBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBook.Name = "tsbBook";
            this.tsbBook.Size = new System.Drawing.Size(60, 22);
            this.tsbBook.Text = "经典小说";
            this.tsbBook.Click += new System.EventHandler(this.tsbBook_Click);
            this.tsbBook.DoubleClick += new System.EventHandler(this.tsbBook_DoubleClick);
            // 
            // tsbRank
            // 
            this.tsbRank.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbRank.Image = ((System.Drawing.Image)(resources.GetObject("tsbRank.Image")));
            this.tsbRank.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRank.Name = "tsbRank";
            this.tsbRank.Size = new System.Drawing.Size(60, 22);
            this.tsbRank.Text = "小说排行";
            this.tsbRank.Click += new System.EventHandler(this.tsbRank_Click);
            // 
            // tsbMyBook
            // 
            this.tsbMyBook.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbMyBook.Image = ((System.Drawing.Image)(resources.GetObject("tsbMyBook.Image")));
            this.tsbMyBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMyBook.Name = "tsbMyBook";
            this.tsbMyBook.Size = new System.Drawing.Size(60, 22);
            this.tsbMyBook.Text = "我的书架";
            this.tsbMyBook.Click += new System.EventHandler(this.tsbMyBook_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbNew
            // 
            this.tsbNew.Image = ((System.Drawing.Image)(resources.GetObject("tsbNew.Image")));
            this.tsbNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNew.Name = "tsbNew";
            this.tsbNew.Size = new System.Drawing.Size(76, 22);
            this.tsbNew.Text = "新增书籍";
            this.tsbNew.Click += new System.EventHandler(this.tsbNew_Click);
            // 
            // tsbAdd
            // 
            this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
            this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdd.Name = "tsbAdd";
            this.tsbAdd.Size = new System.Drawing.Size(76, 22);
            this.tsbAdd.Text = "加入书架";
            this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbFullScreen
            // 
            this.tsbFullScreen.Image = ((System.Drawing.Image)(resources.GetObject("tsbFullScreen.Image")));
            this.tsbFullScreen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFullScreen.Name = "tsbFullScreen";
            this.tsbFullScreen.Size = new System.Drawing.Size(52, 22);
            this.tsbFullScreen.Text = "全屏";
            this.tsbFullScreen.Click += new System.EventHandler(this.tsbFullScreen_Click);
            // 
            // tsbVoice
            // 
            this.tsbVoice.Image = ((System.Drawing.Image)(resources.GetObject("tsbVoice.Image")));
            this.tsbVoice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVoice.Name = "tsbVoice";
            this.tsbVoice.Size = new System.Drawing.Size(52, 22);
            this.tsbVoice.Text = "朗读";
            this.tsbVoice.Click += new System.EventHandler(this.tsbVoice_Click);
            // 
            // tsbOnLine
            // 
            this.tsbOnLine.Image = ((System.Drawing.Image)(resources.GetObject("tsbOnLine.Image")));
            this.tsbOnLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOnLine.Name = "tsbOnLine";
            this.tsbOnLine.Size = new System.Drawing.Size(52, 22);
            this.tsbOnLine.Text = "离线";
            this.tsbOnLine.Click += new System.EventHandler(this.tsbOnLine_Click);
            // 
            // tsbSelect
            // 
            this.tsbSelect.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tsbSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsbSelect.Image")));
            this.tsbSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSelect.Name = "tsbSelect";
            this.tsbSelect.Size = new System.Drawing.Size(52, 22);
            this.tsbSelect.Text = "过滤";
            this.tsbSelect.Click += new System.EventHandler(this.tsbSelect_Click);
            // 
            // tsbDown
            // 
            this.tsbDown.Image = ((System.Drawing.Image)(resources.GetObject("tsbDown.Image")));
            this.tsbDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDown.Name = "tsbDown";
            this.tsbDown.Size = new System.Drawing.Size(52, 22);
            this.tsbDown.Text = "下载";
            this.tsbDown.Click += new System.EventHandler(this.tsbDown_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbSave
            // 
            this.tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbSave.Image")));
            this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSave.Name = "tsbSave";
            this.tsbSave.Size = new System.Drawing.Size(52, 22);
            this.tsbSave.Text = "保存";
            this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
            // 
            // tsbSearch
            // 
            this.tsbSearch.BackColor = System.Drawing.SystemColors.Control;
            this.tsbSearch.Image = ((System.Drawing.Image)(resources.GetObject("tsbSearch.Image")));
            this.tsbSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearch.Name = "tsbSearch";
            this.tsbSearch.Size = new System.Drawing.Size(52, 22);
            this.tsbSearch.Text = "搜索";
            this.tsbSearch.Click += new System.EventHandler(this.tsbSearch_Click);
            // 
            // stbUpdate
            // 
            this.stbUpdate.Image = ((System.Drawing.Image)(resources.GetObject("stbUpdate.Image")));
            this.stbUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stbUpdate.Name = "stbUpdate";
            this.stbUpdate.Size = new System.Drawing.Size(52, 22);
            this.stbUpdate.Text = "升级";
            this.stbUpdate.Click += new System.EventHandler(this.stbUpdate_Click);
            // 
            // tsbAbout
            // 
            this.tsbAbout.Image = ((System.Drawing.Image)(resources.GetObject("tsbAbout.Image")));
            this.tsbAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAbout.Name = "tsbAbout";
            this.tsbAbout.Size = new System.Drawing.Size(52, 21);
            this.tsbAbout.Text = "关于";
            this.tsbAbout.Click += new System.EventHandler(this.tsbAbout_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsAdd,
            this.cmsNew,
            this.cmsDown,
            this.cmsSearch,
            this.cmsDel});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(125, 114);
            // 
            // cmsAdd
            // 
            this.cmsAdd.Name = "cmsAdd";
            this.cmsAdd.Size = new System.Drawing.Size(124, 22);
            this.cmsAdd.Text = "加入书架";
            this.cmsAdd.Click += new System.EventHandler(this.tsbAdd_Click);
            // 
            // cmsNew
            // 
            this.cmsNew.Name = "cmsNew";
            this.cmsNew.Size = new System.Drawing.Size(124, 22);
            this.cmsNew.Text = "新增小说";
            this.cmsNew.Click += new System.EventHandler(this.tsbNew_Click);
            // 
            // cmsDown
            // 
            this.cmsDown.Name = "cmsDown";
            this.cmsDown.Size = new System.Drawing.Size(124, 22);
            this.cmsDown.Text = "下载打包";
            this.cmsDown.Click += new System.EventHandler(this.tsbDown_Click);
            // 
            // cmsSearch
            // 
            this.cmsSearch.Name = "cmsSearch";
            this.cmsSearch.Size = new System.Drawing.Size(124, 22);
            this.cmsSearch.Text = "搜索本书";
            this.cmsSearch.Click += new System.EventHandler(this.tsbSearch_Click);
            // 
            // cmsDel
            // 
            this.cmsDel.Name = "cmsDel";
            this.cmsDel.Size = new System.Drawing.Size(124, 22);
            this.cmsDel.Text = "删除本书";
            this.cmsDel.Click += new System.EventHandler(this.cmsDel_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 605);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "清风小说浏览器";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        /// <summary>
        /// 下方的工作状态
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel workStatus;
        private System.Windows.Forms.WebBrowser myBrowser;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton tsbAdd;
        /// <summary>
        /// 下载按钮。
        /// </summary>
        public  System.Windows.Forms.ToolStripButton tsbDown;
        private System.Windows.Forms.ToolStripButton tsbFullScreen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbOnLine;
        private System.Windows.Forms.ToolStripButton tsbSearch;
        private System.Windows.Forms.ToolStripButton tsbBook;
        private System.Windows.Forms.ToolStripButton tsbRank;
        private System.Windows.Forms.ToolStripButton tsbMyBook;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel readTip;
        private System.Windows.Forms.ToolStripStatusLabel onlineStatus;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem cmsNew;
        private System.Windows.Forms.ToolStripMenuItem cmsAdd;
        private System.Windows.Forms.ToolStripMenuItem cmsDel;
        private System.Windows.Forms.ToolStripMenuItem cmsDown;
        private System.Windows.Forms.ToolStripButton tsbNew;
        private System.Windows.Forms.ToolStripButton tsbSave;
        private System.Windows.Forms.ToolStripMenuItem cmsSearch;
        private System.Windows.Forms.ToolStripStatusLabel softversion;
        private System.Windows.Forms.ToolStripStatusLabel regVersion;
        private System.Windows.Forms.ToolStripButton stbUpdate;
        /// <summary>
        /// 结构树
        /// </summary>
        public System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ToolStripStatusLabel selectType;
        private System.Windows.Forms.ToolStripButton tsbSelect;
        private System.Windows.Forms.ToolStripButton tsbVoice;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbAbout;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;

    }
}

