﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace BookOnline
{
    /// <summary>
    /// 增加新书窗口类
    /// </summary>
    public partial class addform : Form
    {
        private OleDbConnection odcConnection;
        string sBookType;
        int iAddType;
        mainForm myForm;
        ContextMenuStrip contextMenuStrip;
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="odcConn">数据库连接</param>
        /// <param name="sbooktype">书籍的类型，下拉菜单的默认值</param>
        /// <param name="iType">增加时候的类型，1同时添加到个人书架，2仅仅添加到总书库</param>
        ///<param name="contextmenustrip">右键菜单</param>
        ///<param name="treeview">树结构</param>
        public addform(OleDbConnection odcConn,string sbooktype,int iType,mainForm myform, ContextMenuStrip contextmenustrip)
        {
            InitializeComponent();
            odcConnection = odcConn;
            
            sBookType = sbooktype;
            
            iAddType = iType;
            myForm = myform;
            contextMenuStrip = contextmenustrip;
            //界面的初始化一定要在显示窗口以前，否则不能激发重画窗口的动作
            this.getBookType();
            this.btAdvance_Click(this, null);
            this.ShowDialog();
        }
        /// <summary>
        /// 重载
        /// </summary>
        /// <param name="bookname">书名</param>
        /// <param name="bookindex">书籍目录</param>
        /// <param name="odcConn">连接</param>
        /// <param name="sbooktype">类型</param>
        /// <param name="iType">是否全部添加</param>
        /// <param name="treeview">树枝</param>
        /// <param name="contextmenustrip">右键菜单</param>
        public addform(string bookname,string bookindex,OleDbConnection odcConn, string sbooktype, int iType,mainForm myform , ContextMenuStrip contextmenustrip)
        {
            InitializeComponent();
            odcConnection = odcConn;
            sBookType = sbooktype;
            iAddType = iType;
            myForm = myform;
            contextMenuStrip = contextmenustrip;
            //界面的初始化一定要在显示窗口以前，否则不能激发重画窗口的动作
            this.getBookType();
            txBookName.Text = bookname;
            txBookURL.Text = bookindex;
            this.btAdvance_Click(this, null);
            this.ShowDialog();
        }
        private void getBookType(){

            OleDbDataReader odrReader;
            try
            {
                odcConnection.Open();

                OleDbCommand odCommand = odcConnection.CreateCommand();
                //3、输入查询语句
                odCommand.CommandText = "select rtrim(TypeName) as  BookType,rtrim(TypeID) as TypeID from BookType order by TypeID";

                //建立读取
                odrReader = odCommand.ExecuteReader();

                while (odrReader.Read())
                {

                    cbBookType.Items.Add(odrReader["BookType"].ToString());
                  
                }
                //这里的选择是要用selectitem
                cbBookType.SelectedItem = sBookType;
                odrReader.Close();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                odcConnection.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sbooktype = cbBookType.SelectedItem.ToString().Trim();
            string sbookname=txBookName.Text.Trim();
            string sbookurl=txBookURL.Text.Trim();

            string sbookinfo = txBookInfo.Text;
            string sbookwriter = txBookWriter.Text;

            TreeNode tnNew; // 要增加的书籍类型结点
            TreeNode tnNewSon;//要增加的书籍结点
            if (sbooktype == "" || sbookname == "" || sbookurl == "")
            {
                MessageBox.Show("请检查必填字段是否填写完整。");
                return;
            }

            int iRow;//受影响的行数
            try
            {
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "insert into [Book]([BookName],[Writer],[TypeID],[BookURL],[Describe]) select '"+sbookname+"','"+sbookwriter+"',BookType.TypeID,'"+sbookurl+"','"+sbookinfo+"' from [BookType] where [TypeName]='" + sbooktype + "'";
                //建立读取
                iRow = odCommand.ExecuteNonQuery();

                if (iRow == 1)
                {
                    int iTypeIndex;
                    iTypeIndex = myForm.treeView.Nodes.IndexOfKey(sbooktype);
                    //在指定类型下面增加书点,在书架页面的话，要先判断是不是有这种图书类型，没有的话就增加一个
                    if (iTypeIndex == -1)//添加进去了并且没有找到书籍类型
                    {

                        tnNew = myForm.treeView.Nodes.Add(sbooktype);
                        tnNew.Name = sbooktype;
                        tnNew.ToolTipText = sbooktype;
                        tnNew.Text = sbooktype;
                        tnNew.SelectedImageIndex = 3;
                        tnNew.ImageIndex = 3;
                        tnNew.ContextMenuStrip = contextMenuStrip;
                        tnNewSon = tnNew.Nodes.Add(sbookname);
                    }
                    else
                    {
                        tnNewSon = myForm.treeView.Nodes[iTypeIndex].Nodes.Add(sbookname);
                        tnNew = myForm.treeView.Nodes[iTypeIndex];
                    }
                    tnNewSon.Name = sbookname;
                    tnNewSon.ToolTipText = sbookname;
                    tnNewSon.Text = sbookname;
                    tnNewSon.Tag = sbookurl;
                    tnNewSon.SelectedImageIndex = 1;
                    tnNewSon.ImageIndex = 2;
                    tnNewSon.ContextMenuStrip = contextMenuStrip;
                    tnNew.Text = tnNew.ToolTipText + "(" + tnNew.Nodes.Count + ")";

                    if (iAddType == 1)
                    {
                        odCommand.CommandText = "insert into [MyBook]([BookName]) values('" + sbookname + "')";
                        //添加进去
                        odCommand.ExecuteNonQuery();
                        //书库里面也要添加进去
                        iTypeIndex = myForm.tvBook.Nodes.IndexOfKey(sbooktype);
                        tnNewSon = myForm.tvBook.Nodes[iTypeIndex].Nodes.Add(sbookname);
                        tnNew = myForm.tvBook.Nodes[iTypeIndex];

                        tnNewSon.Name = sbookname;
                        tnNewSon.ToolTipText = sbookname;
                        tnNewSon.Text = sbookname;
                        tnNewSon.Tag = sbookurl;
                        tnNewSon.SelectedImageIndex = 1;
                        tnNewSon.ImageIndex = 2;
                        tnNewSon.ContextMenuStrip = contextMenuStrip;
                        tnNew.Text = tnNew.ToolTipText + "(" + tnNew.Nodes.Count + ")";
                    }


                }
                else
                    MessageBox.Show("书籍添加失败");


                //一本添加完成后清空重置窗口
                cbBookType.SelectedItem = sBookType;
                txBookName.Text="";
                txBookWriter.Text="";
                txBookURL.Text="";
                txBookInfo.Text="";
                if (cbMany.Checked != true)
                    this.Hide();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                odcConnection.Close();
            }

        }
        private bool checkBookName(string sbookname)
        {

            OleDbDataReader odrReader;
            try
            {
                bool bResult = true;
                odcConnection.Open();

                OleDbCommand odCommand = odcConnection.CreateCommand();

                odCommand.CommandText = "select * from Book where BookName='" + sbookname + "'";

                //建立读取
                odrReader = odCommand.ExecuteReader();

                if (odrReader.Read())
                    bResult = false;

                odrReader.Close();
                return bResult;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {

                odcConnection.Close();
            }
        }

        private void txBookName_Leave(object sender, EventArgs e)
        {
            if (txBookName.Text.Trim() != "" && checkBookName(txBookName.Text.Trim()) == false)
            {
                MessageBox.Show("该书名已经存在。");
                txBookName.Focus();
            }
       
        }

        private void txBookURL_Leave(object sender, EventArgs e)
        {
            string sbookurl = txBookURL.Text.Trim();
            if (sbookurl != "")
            {
                Match match = Regex.Match(sbookurl, "http://\\S+/\\S*", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                if (!match.Success)
                {
                    MessageBox.Show("网址不符合要求，请参考：以http://www.lieshu.net/bookdir/bookindex.htm");
                    txBookURL.Focus();
                }

            }           

        }

        private void btAdvance_Click(object sender, EventArgs e)
        {
            if (btAdvance.Text =="<<高级")
            {
                lbBookInfo.Hide();
                txBookInfo.Hide();
                lbBookWriter.Hide();
                txBookWriter.Hide();
                btOk.Top = 194;
                btCancel.Top = 194;
                btAdvance.Top = 194;
                lbBookURL.Top = 143;
                txBookURL.Top = 143;
                this.Height = 268;
                btAdvance.Text = "高级>>";
            }
            else
            {
                lbBookInfo.Show();
                txBookInfo.Show();
                lbBookWriter.Show();
                txBookWriter.Show();
                btOk.Top = 393;
                btCancel.Top = 393;
                btAdvance.Top = 393;
                lbBookURL.Top = 194;
                txBookURL.Top = 194;
                this.Height = 467;
                btAdvance.Text = "<<高级";
            }



        }
    }
}