﻿// JavaScript Document
//定义几个页面
var prepage;
var nextpage;
var homepage;
var endpage;

//是否接受回车返回书目
var enableEnter = true;

document.oncontextmenu=new Function("showmenu(event);event.returnValue=false;");
document.onkeydown=new Function("pageEvent(event);");
document.onclick=hidemenu;
//获得某个控件的简化版本
function A(id)
 {
 return document.getElementById(id);
 } 
//各种转向，到上一页，下一页，书目
function goNext()
{
 window.location.href=nextpage;
 return false; 
}
function goPre()
{
 window.location.href=prepage;
 return false; 
}
function goHome()
{
 window.location.href=homepage;
 return false; 
}
function goEnd()
{
 window.location.href=endpage;
 return false; 
}
function goBack()
{
 window.history.back();
 return false; 
}
function goMyIndex()
{
 window.location.href="http://www.mxstdio.cn/";
 return false; 
}
//页面时间处理
function pageEvent(event) 
{ 
//判断是否有无事件
 	event = event ? event : (window.event ? window.event : null); 
 	if (event.keyCode==13)
 	{
 		if(enableEnter)
 		{
 			window.location.href=bookpage;
 			return false;
 		}
 	} 
    //Home键
	if (event.keyCode==36)
		window.location.href=homepage;
	//End键
	if (event.keyCode==35)
		window.location.href=endpage;
	//左键
 	if (event.keyCode==37) 
		window.location.href=prepage;
    //右键
 	if (event.keyCode==39) 
		window.location.href=nextpage;
} 
//画出右键窗口
function drawmenu(){
	document.write("<div id='menu' onclick='hidemenu()'>");
	document.write("<div class='side'></div>");		
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='return goNext();'>");
	document.write("<img src='../../model/icon/next.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下&nbsp;&nbsp;章");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='return goPre();'>");
	document.write("<img src='../../model/icon/pre.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上&nbsp;&nbsp;章");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='return goBack();'>");
	document.write("<img src='../../model/icon/back_book.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;后&nbsp;&nbsp;退");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='return goEnd();'>");
	document.write("<img src='../../model/icon/next.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;末&nbsp;&nbsp;页");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='return goHome();'>");
	document.write("<img src='../../model/icon/back_items.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;目&nbsp;&nbsp;录");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='return goMyIndex();'>");
	document.write("<img src='../../model/icon/back_home.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关&nbsp;&nbsp;于");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='window.location.reload();'>");
	document.write("<img src='../../model/icon/refresh.gif' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;刷&nbsp;&nbsp;新");
	document.write("</div>");
	document.write("<div class='menuitems' onmouseover='myover(this);' onmouseout='myout(this);' onclick='javascript:window.print();'>");
	document.write("<img src='../../model/icon/print.gif' width='12' height='12'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打&nbsp;&nbsp;印");
	document.write("</div>");
	document.write("</div>");
}
//显示右键菜单
function showmenu(event)
{

	isShowRightMenu=true;
	event = event ? event : (window.event ? window.event : null); 
	var menu = A("menu");
		
	menu.style.display="block"; 


	//右边距
 	var rightedge=document.documentElement.clientWidth-event.clientX;
	//下边距
	var bottomedge=document.documentElement.clientHeight-event.clientY;
	//左越界
 	if(rightedge<menu.offsetWidth)
		menu.style.left=document.documentElement.scrollLeft+event.clientX-menu.offsetWidth;
    else
 		menu.style.left=document.documentElement.scrollLeft+event.clientX;
	//下越界
    if(bottomedge<menu.offsetHeight)
 		menu.style.top=document.documentElement.scrollTop+event.clientY-menu.offsetHeight;
 	else 
 		menu.style.top=document.documentElement.scrollTop+event.clientY; 

		
    return false;
	
}
//隐藏
 function hidemenu()
 {
	var menu = A("menu"); 
	menu.style.display="none";
 }
//鼠标在上面的时候
 function myover(obj)
{
	 obj.className = "itemshovor";
}
//鼠标离开的时候
function myout(obj)
{
	obj.className = "menuitems";
}