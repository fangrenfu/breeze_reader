﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BookOnline
{
   /// <summary>
   /// 打包对话框
   /// </summary>
    public partial class PacketForm : Form
    {

        int iPacketType; //打包类型 0为没有选择,1为Txt，2为CHM

        /// <summary>
       /// 初始化
       /// </summary>
        public PacketForm()
        {
            InitializeComponent();
            iPacketType = 0;
            this.ShowDialog();
        }
        private void rbTxt_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTxt.Checked == true)
            {
                rbChm.Checked = false;
            }
        }

        private void rbChm_CheckedChanged(object sender, EventArgs e)
        {
            if (rbChm.Checked == true)
                rbTxt.Checked = false;
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            if (rbTxt.Checked == true)
                iPacketType = 1;

            if (rbChm.Checked == true)
                iPacketType = 2;
            this.Hide();
        }
        /// <summary>
        /// 获得打包类型
        /// </summary>
        /// <returns>0为没有选择,1为Txt，2为CHM</returns>
        public int getPacketType()
        {
            return iPacketType;
        }
    }
}