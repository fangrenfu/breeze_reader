using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data.OleDb;
using System.Threading;
using SpeechLib;

namespace BookOnline
{
    /// <summary>
    /// 主窗口类
    /// </summary>
    public partial class mainForm : Form
    {
        //以下用于存储正则表达式
        string sBookName;//书籍名称
        string sBookURL;//书籍目录地址
        string sBookURLHead;//书目的前面部分
        string sBookURLFoot;//书目的后半部分

        string sSiteHost;//主机部分
        string sBookType;//当前书籍的类型
        TreeNode tnBook=null;//当前书籍
        TreeNode tnBookType = null; //当前书籍的上一层。
        public TreeView tvBook;//存储经典书籍库树
        TreeView tvRank;//存储排行榜树
        TreeView tvMyBook;//存储我的书架树


        string sURL ;//用于当前访问的网页地址
        string sURLPath ;//当前访问地址的前面部分
        string sSaveFileName ;//当前访问网址的后面部
        string sSaveDir ;//下载后文件保存的目录
        string sCurrentFile ;//当前浏览的本地地址。
        string sCurrentTitle;//访问页面的标题
        bool bFileDown = false;//文件是否已经下载
        bool bOnline = true;//是否在线
        bool bSelect = true;//是否过滤
        bool bVoice = false;//是否加载语音朗读

        string sScrollSpeed;//阅读页滚动速度
        string sBackColor;//阅读页背景颜色
        string sFontSize; //字体大小
        string sTextColor; //文字颜色

        string SearchURL = "http://www.lieshu.net/?V1.1.2search";
        string StartURL = "http://www.fangrenfu.me/";
        string SoftName = "清风小说浏览器";

        Thread tDownload = null;//下载线程

        ArrayList asChapterList = new ArrayList();//章节列表
        ArrayList asBookReg= new ArrayList();//书籍正则表达式
        ArrayList asReadSet = new ArrayList();//阅读设置

        about aboutSoft = new about();//关于

        SpVoiceClass sp;

        OleDbConnection odcConnection;
        OleDbConnection odcConnectionConfig;
        //数据库连接参数
        string sAccessUser = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\user;Jet OLEDB:Database Password=comefirstfangrenfu;";
        string sAccessConfig = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\config;Jet OLEDB:Database Password=comefirstfangrenfu;";
        /// <summary>
        /// 主窗口函数，进行初始化窗口
        /// </summary>
        public mainForm()
        {
            InitializeComponent();
            
            softversion.Text = "版本:1.1.2绿色版";

            //初始化树
            tvBook = new TreeView();
            tvMyBook = new TreeView();
            tvRank = new TreeView();

         
            //将窗口最大化
            this.WindowState = FormWindowState.Maximized;
           // sp = new SpVoiceClass();//初始化语音类
            //建立数据库连接
             odcConnection= new OleDbConnection(sAccessUser);
             odcConnectionConfig = new OleDbConnection(sAccessConfig);
             this.Text = SoftName;
             sBookName = "";
             InitReadSetting();
             getRegVerison();
             tsbBook_Click(this,EventArgs.Empty);
             myBrowser.Navigate(StartURL);
        }
        //获得特征库的版本
        private void getRegVerison()
        {
            try
            {
                OleDbDataReader odrReader;
                odcConnectionConfig.Open();

                OleDbCommand odCommand = odcConnectionConfig.CreateCommand();
                //3、输入查询语句
                odCommand.CommandText = "SELECT Top 1 format(Version,'yyyy-mm-dd') as Version from Ver";

                //建立读取
                odrReader = odCommand.ExecuteReader();
                //读取成功的话
                if (odrReader.Read())
                {
                     regVersion.Text= "特征库:"+odrReader["Version"].ToString();
                }
                odrReader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                odcConnection.Close();
            }
            finally
            {
                odcConnectionConfig.Close();
            }
        }
        //复制一个TreeView到另一个支持两层
        private void cloneTreeView(TreeView tvOld, TreeView tvNew)
        {
            try
            {
                TreeNode tnNew;
                TreeNode tnNewSon;
                TreeNode tnOld;
                tvNew.Nodes.Clear();

                for (int i = 0; i < tvOld.Nodes.Count; i++)
                {
                    tnNew = tvNew.Nodes.Add(tvOld.Nodes[i].Name);
                    tnNew.Name = tvOld.Nodes[i].Name;
                    tnOld = tvOld.Nodes[i];
                    tnNew.Tag = tvOld.Nodes[i].Tag;
                    tnNew.ToolTipText = tvOld.Nodes[i].ToolTipText;
                    tnNew.Text = tvOld.Nodes[i].Text;
                    tnNew.ImageIndex = tvOld.Nodes[i].ImageIndex;
                    tnNew.SelectedImageIndex = tvOld.Nodes[i].SelectedImageIndex;
                    tnNew.ContextMenuStrip = tvOld.Nodes[i].ContextMenuStrip;
                    for (int j = 0; j < tnOld.Nodes.Count; j++)
                    {
                        tnNewSon = tnNew.Nodes.Add(tnOld.Nodes[j].Name);
                        tnNewSon.Name = tnOld.Nodes[j].Name;
                        tnNewSon.Tag = tnOld.Nodes[j].Tag;
                        tnNewSon.ToolTipText=tnOld.Nodes[j].ToolTipText;
                        tnNewSon.Text=tnOld.Nodes[j].Text;
                        tnNewSon.ImageIndex = tnOld.Nodes[j].ImageIndex;
                        tnNewSon.SelectedImageIndex = tnOld.Nodes[j].SelectedImageIndex;
                        tnNewSon.ContextMenuStrip = tnOld.Nodes[j].ContextMenuStrip;
                    }
                    if (tvOld.Nodes[i].IsExpanded)
                        tnNew.Expand();
                    
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                workStatus.Text = ("复制树失败！");
            }
        }
        //初始化阅读设置
        private void InitReadSetting()
        {
            
            try
            {
                OleDbDataReader odrReader;
                odcConnection.Open();

                OleDbCommand odCommand = odcConnection.CreateCommand();
                //3、输入查询语句
                odCommand.CommandText = "SELECT Top 1 ScrollSpeed,rtrim(BackColor) as BackColor, FontSize, rtrim(TextColor) as TextColor from ReadSet";

                //建立读取
                odrReader = odCommand.ExecuteReader();
                //读取成功的话
                if (odrReader.Read())
                {
                    sScrollSpeed = odrReader["ScrollSpeed"].ToString();
                    sBackColor = odrReader["BackColor"].ToString();
                    sFontSize = odrReader["FontSize"].ToString();
                    sTextColor = odrReader["TextColor"].ToString();
                    asReadSet.Clear();
                    asReadSet.Add(sScrollSpeed);
                    asReadSet.Add(sBackColor);
                    asReadSet.Add(sFontSize);
                    asReadSet.Add(sTextColor);
                }
                odrReader.Close();
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                //MessageBox.Show("获取阅读设置:" + ex.Message + ex.Data);
                workStatus.Text = ("读取阅读设置失败");
            }
            finally
            {
                odcConnection.Close();
            }
        }
        //当打开新窗口的时候不反应。
        private void myBrowser_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }
        //处理新增书籍
        private void AddBook(string URL)
        {
            string sitename;
            string bookindex;
            string bookname;
            if(tsbMyBook.BackColor != System.Drawing.SystemColors.ControlLightLight)
                tsbMyBook_Click(this,EventArgs.Empty);
            Match match = Regex.Match(URL,"sitename=([^&]*)&bookindex=([^&]*)&bookname=([^&]*)&" , RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (match.Success)
            {
                sitename = match.Groups[1].Value;
                bookindex = match.Groups[2].Value;
                bookname = match.Groups[3].Value;
                addform addbook = new addform(bookname+'-'+sitename,bookindex,odcConnection,sBookType, 1, this, contextMenuStrip);
            }       
        }
        //在载入之前截取事件
        private void getVisitSetting(string sBookURL)
        {
            sBookURLHead = getFileFromWeb.getPathFromURL(sBookURL);
            sSiteHost = getFileFromWeb.getHostFromURL(sBookURL);
            asBookReg = bookOperate.getBookReg(sSiteHost, odcConnectionConfig);
            sBookURLFoot = asBookReg[5].ToString();
            sBookName = "临时访问";
           
            sSaveDir = Application.StartupPath + "\\book\\" + sBookName + "\\";
            string sHtml = getFileFromWeb.getHtml(sBookURLHead+sBookURLFoot, "");
            asChapterList = buildHtml.makeIndex(sHtml, sBookURLHead, sSaveDir, sBookURLFoot, asBookReg, asReadSet);

        }
        private void myBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            string sURLtmp = e.Url.ToString(); ;
            string sURLPathtmp = getFileFromWeb.getPathFromURL(sURLtmp) ;
            string sSaveFileNametmp = getFileFromWeb.getNameFromURL(sURLtmp);
            string sHostTmp = getFileFromWeb.getHostFromURL(sURLtmp);


            if (sURLtmp.IndexOf("about") == 0 || sHostTmp == "http://www.mxstdio.cn/" || sHostTmp == "http://www.KieeSoft.cn/")
                return;
       
            if (sHostTmp == "http://www.lieshu.net/")
            {
                if (sURLtmp.IndexOf("Action=addindex") != -1)//是添加到清风书架命令
                {
                    AddBook(sURLtmp);
                    e.Cancel = true;
                    return;
                }
                else
                    return;
            }
   
            if (sURLtmp.IndexOf("res://") != 0)
            {
                sURL = sURLtmp;
                sURLPath = sURLPathtmp;
                sSaveFileName = sSaveFileNametmp;
                sSiteHost=sHostTmp;
            }
            else
            {
                e.Cancel = true;
                return;//否则返回程序
            }

            if (sURL.IndexOf("http://") == 0)//如果访问外网,检查是否与当前的设定相同。
            {
                if (sBookURLHead!=sURLPath)//发生变化了，很可能是猎书网那边过去的。
                {
                   getVisitSetting(sURL);
                }
                e.Cancel = true;
            }
                
            if (bFileDown == false)
            {
                //不是原始主页
                if (sSaveFileName!=sBookURLFoot)
                    sSaveFileName=getFileFromWeb.changeToVisitFormat(sSaveFileName);
                sURL = sBookURLHead + sSaveFileName;
                //下载文件内容
                string sHtml = getFileFromWeb.getHtml(sURL, "");
                if (sHtml == "\n")//如果没有返回任何信息,退出程序
                    return;
                //如果这个时候目录文件

                sSaveFileName = getFileFromWeb.changeToSaveFormat(sSaveFileName);
                if (sURL == sBookURLHead+sBookURLFoot)
                    asChapterList = buildHtml.makeIndex(sHtml, sBookURLHead, sSaveDir, sSaveFileName, asBookReg,asReadSet);
                else
                {
                    sCurrentTitle= buildHtml.makeContent(sHtml, sBookName, sSaveDir, sSaveFileName, sBookURLHead, sBookURLFoot, asBookReg, asChapterList, asReadSet);
                }

                bFileDown = true;
                //暂时取消掉。
                this.myBrowser.DocumentCompleted -= new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.myBrowser_DocumentCompleted);
                myBrowser.Navigate(Application.StartupPath + "\\book\\" + sBookName + "\\" + sSaveFileName);
            }
            else
            {
                bFileDown = false;
                this.myBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.myBrowser_DocumentCompleted);
            }
        }
        private void readTxtFile(string sFilePath)
        {
            string sHtml;
            StreamReader sr = new StreamReader(sFilePath);
            try
            {
               
                sHtml = sr.ReadToEnd();
                sr.Close();                
                Match matchContent = Regex.Match(sHtml, "<!--content start-->([^\v]*)<!--content end-->", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                //替换所有的<p>为回车换行
                sHtml = matchContent.Groups[1].Value;
                sHtml = Regex.Replace(sHtml, "<P>", "\r\n  ", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                //向前跳过9999个句子，目前只支持Sentence这个类型。
                SpeechLib.ISpeechObjectTokens sps = sp.GetVoices("Language = 804", "");

                if (sps.Count > 0)
                {
                    sp.Voice = sps.Item(0);
                }
                else//如果没有中文的语音引擎，提示并且退出
                {
                    MessageBox.Show("没有找到中文语音引擎，请先安装！");
                    return;
                }

                sp.Skip("Sentence", 9999);
                sp.Speak(sHtml, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
            catch (Exception ex)
            {
                string message = ex.Message.ToString();
                //MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                sr = null;
            }
        }
        private void tsbBook_DoubleClick(object sender, EventArgs e)
        {
            //双击经典小说按钮后产生的事件，主要操作
            //1、清空treeView
            //2、读取数据库中的小说类别,具体小说。
            //3、填充treeView
        }
        private void buildBookTree(string typename)
        {
            try
            {
                OleDbDataReader odrReader;

                treeView.Nodes.Clear();

                odcConnection.Open();

                OleDbCommand odCommand = odcConnection.CreateCommand();
                //3、输入查询语句

                //区别两种按钮,book为经典书库,mybook为我的书架
                switch (typename)
                {
                    case "经典小说":
                        odCommand.CommandText = "SELECT rtrim(Book.BookName) as BookName, rtrim(Book.BookURL) as BookURL, rtrim(BookType.TypeName) as BookType FROM BookType LEFT JOIN Book ON BookType.TypeID = Book.TypeID ORDER BY BookType.TypeID,Book.BookName";
                        break;
                    case "我的书架":
                        odCommand.CommandText = "select rtrim(Book.BookName) as BookName,rtrim(BookType.TypeName) as BookType, rtrim(Book.BookURL) as BookURL FROM (Book INNER JOIN BookType ON Book.TypeID = BookType.TypeID) INNER JOIN MyBook ON Book.BookName = MyBook.BookName order by BookType.TypeID,Book.BookName ";
                        break;
                    default://默认为我的书架                     
                        break;
                }

                odrReader = odCommand.ExecuteReader();

                string sTypetmp = "";//临时用于比较，比便于看到是否该类已经结束

                //查询并显示数据
                TreeNode tnNew = null;
                TreeNode tnNewSon = null;
                int iBookCount = 0;//总共的书数量
                int iTypeBookCount = 0;//该类书的数量
                while (odrReader.Read())
                {

                    if (sTypetmp != odrReader["BookType"].ToString())
                    {
                        tnNew = treeView.Nodes.Add(odrReader["BookType"].ToString());
                        tnNew.Name = odrReader["BookType"].ToString();
                        tnNew.ToolTipText = odrReader["BookType"].ToString();
                        tnNew.Text = tnNew.ToolTipText + "(0)";
                        tnNew.ContextMenuStrip = contextMenuStrip;
                        tnNew.ImageIndex = 3;
                        tnNew.SelectedImageIndex = 3;
                        iTypeBookCount = 0;
                    }
                    if (odrReader["BookName"].ToString() != "")
                    {
                        tnNewSon = tnNew.Nodes.Add(odrReader["BookName"].ToString());
                        tnNewSon.Name = odrReader["BookName"].ToString();
                        tnNewSon.ToolTipText = odrReader["BookName"].ToString();
                        tnNewSon.Tag = odrReader["BookURL"].ToString();
                        tnNewSon.ContextMenuStrip = contextMenuStrip;
                        sTypetmp = odrReader["BookType"].ToString();
                        tnNewSon.SelectedImageIndex = 1;
                        tnNewSon.ImageIndex = 2;
                        iBookCount++;
                        iTypeBookCount++;
                        tnNew.Text = tnNew.ToolTipText + "(" + iTypeBookCount + ")";
                    }
                }
                workStatus.Text = typename+"-共计" + iBookCount + "本";
                odrReader.Close();

            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                // MessageBox.Show("获取经典小说:"+ex.Message+ex.Data);
                workStatus.Text = "获取小说列表出错！";
            }
            finally
            {
                odcConnection.Close();
            }
               
           
        }
        private void tsbBook_Click(object sender, EventArgs e)
        {

            if(tsbMyBook.BackColor == System.Drawing.SystemColors.ControlLightLight)
                cloneTreeView(treeView, tvMyBook);
            if (tsbRank.BackColor == System.Drawing.SystemColors.ControlLightLight)
                cloneTreeView(treeView, tvRank);

            tsbBook.BackColor = System.Drawing.SystemColors.ControlLightLight;
            tsbMyBook.BackColor = System.Drawing.SystemColors.Control;
            tsbRank.BackColor = System.Drawing.SystemColors.Control;

            if (splitContainer.SplitterDistance == 0)
                tsbFullScreen_Click(this, EventArgs.Empty);

            if (tvBook.Nodes.Count > 0)
            {
                cloneTreeView(tvBook, treeView);
            }
            else
            {
                buildBookTree("经典小说");
            }
        }

        private void tsbMyBook_Click(object sender, EventArgs e)
        {

            if (tsbBook.BackColor == System.Drawing.SystemColors.ControlLightLight)
                cloneTreeView(treeView,tvBook);
           
            if (tsbRank.BackColor == System.Drawing.SystemColors.ControlLightLight)
                cloneTreeView(treeView,tvRank);

            tsbBook.BackColor = System.Drawing.SystemColors.Control;
            tsbMyBook.BackColor = System.Drawing.SystemColors.ControlLightLight;
            tsbRank.BackColor = System.Drawing.SystemColors.Control;

            if (splitContainer.SplitterDistance == 0)
                tsbFullScreen_Click(this, EventArgs.Empty);

            if (tvMyBook.Nodes.Count > 0)
            {
                cloneTreeView(tvMyBook, treeView);
            }
            else
            {
                buildBookTree("我的书架");
            }         
        }

        private void tsbFullScreen_Click(object sender, EventArgs e)
        {
            
        
            if (splitContainer.SplitterDistance == 0)
            {
                splitContainer.Panel1.Show();
                splitContainer.SplitterDistance = 180;
                tsbFullScreen.BackColor = SystemColors.Control;

            }
            else
            {
                splitContainer.Panel1.Hide();
                splitContainer.SplitterDistance = 0;
                tsbFullScreen.BackColor = SystemColors.ControlLightLight;
            }
        }

        private void tsbAdd_Click(object sender, EventArgs e)
        {
            int iRow;
            TreeNode tnNew;//目录节点
            TreeNode tnNewSon;//书籍节点
            if (sBookName == "" || sBookName == "临时访问")
                MessageBox.Show("无法添加入个人书架,请先添加书籍！");
            try
            {
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "insert into [MyBook]([BookName]) select [BookName] from [Book] where [BookName]='" + sBookName + "' and  not exists (select * from [MyBook] where [MyBook].[BookName]=[Book].[BookName])";
                //建立读取
                iRow = odCommand.ExecuteNonQuery();
                odcConnection.Close();

                if (iRow == 1)
                {
                    //更新mybook的树
                    //先查找是否存在该类型节点，没有的话先添加。
                    int iIndex = tvMyBook.Nodes.IndexOfKey(sBookType);
                    if (iIndex == -1)//没有找到
                    {
                        tnNew = tvMyBook.Nodes.Add(sBookType);
                        tnNew.Name = sBookType;
                        tnNew.ToolTipText = sBookType;
                        tnNew.Tag = sBookType;
                        tnNew.Text = sBookType + "(0)";
                        tnNew.ImageIndex = 3;
                        tnNew.SelectedImageIndex = 3;
                        tnNew.ContextMenuStrip = contextMenuStrip;
                    }
                    else//找到的话
                    {
                        tnNew = tvMyBook.Nodes[iIndex];
                    }
                    //添加子节点
                    tnNewSon = tnNew.Nodes.Add(sBookName);
                    tnNewSon.Name = sBookName;
                    tnNewSon.Tag = sBookURL;
                    tnNewSon.ToolTipText = sBookName;
                    tnNewSon.SelectedImageIndex = 1;
                    tnNewSon.ImageIndex = 2;
                    tnNewSon.ContextMenuStrip = contextMenuStrip;
                    tnNew.Text = tnNew.ToolTipText + "(" + tnNew.Nodes.Count + ")";

                }
                else
                {
                    MessageBox.Show("添加到书库失败");
                }
            }
            catch (Exception ex)
            {
                string info = "addBook" + ex.Message + ex.Data;
                // MessageBox.Show(info);
            }

            finally
            {
                odcConnection.Close();
            }
        }
        private void tsbOnLine_Click(object sender, EventArgs e)
        {
            if (bOnline == true)
            {
                this.myBrowser.Navigating -= new WebBrowserNavigatingEventHandler(this.myBrowser_Navigating);
                tsbOnLine.BackColor = SystemColors.ControlLightLight;
                onlineStatus.Text = "状态：离线";
                bOnline = false;

            }
            else
            {
                this.myBrowser.Navigating += new WebBrowserNavigatingEventHandler(this.myBrowser_Navigating);
                tsbOnLine.BackColor = SystemColors.Control;
                onlineStatus.Text = "状态：在线";
                bOnline = true;
            }
        }
        //批量下载文件
        private void tsbDown_Click(object sender, EventArgs e)
        {

            if (sBookName == "" || sBookName == "临时访问")
            {
                MessageBox.Show("请先选择要下载的书籍。如果没有加入书库，请先添加入库");
                return;
            }

            
            if (tDownload == null || !tDownload.IsAlive) //如果线程还没有初始化或者线程不存在
            {

                ArrayList asBookSet = new ArrayList();
                asBookSet.Clear();
                asBookSet.Add(sScrollSpeed);
                asBookSet.Add(sBackColor);
                asBookSet.Add(sFontSize);
                asBookSet.Add(sTextColor);
                PacketForm myform = new PacketForm();
                int ipackettype=myform.getPacketType();
                if (ipackettype != 0)//如果返回的类型不为0，则开始操作
                {
                    bookOperate instance = new bookOperate(this, sBookName, sBookURLHead, sBookURLFoot, Application.StartupPath + "\\book\\" + sBookName + "\\", asBookReg, asChapterList, asBookSet, ipackettype);
                    tDownload = new Thread(new ThreadStart(instance.downAndPacket));

                    tDownload.Start();
                    tsbDown.Text = "停止";
                }
            }
            else
            {
                tDownload.Abort();
                tsbDown.Text = "下载";
            }
        }
        
        private void myBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                //书籍的文件夹存在并且有选中过书籍。
                
                if (Directory.Exists(sSaveDir) && tnBook != null)
                {

                    int iNotLoadCount = asChapterList.Count - Directory.GetFiles(sSaveDir).Length + 1;
                    workStatus.Text = "《" + sBookName + "》共有" + asChapterList.Count + "章，已下载" + (Directory.GetFiles(sSaveDir).Length - 1) + "章。";
                    if (iNotLoadCount > 0)
                        tnBook.Text = tnBook.ToolTipText + "(" + iNotLoadCount + ")";
                    else
                        tnBook.Text = tnBook.ToolTipText;
                }
                sCurrentFile = getFileFromWeb.getNameFromURL(e.Url.ToString());
                if (bVoice == true && sCurrentFile != getFileFromWeb.changeToSaveFormat(sBookURLFoot))
                    readTxtFile(Application.StartupPath + "\\book\\" + sBookName + "\\" + sCurrentFile);

                if (sBookName != "")
                    this.Text = SoftName + "-《" + sBookName + "》" + sCurrentTitle;

            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                //MessageBox.Show("DocumentCompleted"+ex.Message+ex.Data);
                workStatus.Text= "网页加载完成时异常！";
            }

        }
        private void treeView_MouseUp(object sender, MouseEventArgs e)
        {
            Point pClick = new Point(e.X, e.Y);
            TreeNode tnClick = this.treeView.GetNodeAt(pClick);
            string sLocalFilePath="";

            if (tnClick != null)//点击在树上的时候才是有效的
            {
                if (tnClick.Parent != null)//如果是具体书籍
                {
                    tnBook = tnClick;
                    tnBookType = tnBook.Parent;
                    sBookURL = tnBook.Tag.ToString();
                    sBookName = tnBook.ToolTipText;
                    sBookType = tnBook.Parent.ToolTipText;
                   
                    sBookURLHead = getFileFromWeb.getPathFromURL(sBookURL);
                    sBookURLFoot = getFileFromWeb.getNameFromURL(sBookURL);
                    sSiteHost=getFileFromWeb.getHostFromURL(sBookURL);
                    
                    asBookReg= bookOperate.getBookReg(sSiteHost, odcConnectionConfig);
                     

                    sLocalFilePath=Application.StartupPath + "\\book\\" + sBookName + "\\" +getFileFromWeb.changeToSaveFormat(sBookURLFoot); //本地文件路径
                    sSaveDir = Application.StartupPath + "\\book\\" + sBookName + "\\";

                    sCurrentTitle = "";
                    
                    //点的是左键，并且不再排行榜中
                    if (e.Button == MouseButtons.Left&&tsbRank.BackColor != System.Drawing.SystemColors.ControlLightLight)
                    {
                        if (bOnline)
                            myBrowser.Navigate(sBookURL);
                        else
                            myBrowser.Navigate(sLocalFilePath);
                    }
             
                    //把新增小说这个项目变成灰色
                    if (tsbRank.BackColor == SystemColors.ControlLightLight)
                    {
                        cmsAdd.Enabled = false;//添加到书架
                        cmsDel.Enabled = false;//删除书籍
                        cmsSearch.Enabled = true;//搜索本书
                        cmsDown.Enabled = false;//下载
                        cmsNew.Enabled = false;//新建
                    }

                    //在只有在书架页面可以进行删除、新建、下载、打包等操作
                    if (tsbMyBook.BackColor == SystemColors.ControlLightLight)
                    {
                        cmsAdd.Enabled = false;
                        cmsDel.Enabled = true;
                        cmsDown.Enabled = true;
                        cmsNew.Enabled = true;
                        cmsSearch.Enabled = true;//搜索本书

                    }
                    if (tsbBook.BackColor == SystemColors.ControlLightLight)
                    {
                        cmsAdd.Enabled = true;
                        cmsDel.Enabled = true;
                        cmsDown.Enabled = true;
                        cmsNew.Enabled = true;
                        cmsSearch.Enabled = true;//搜索本书      
                    }


                }
                else//在目录的话，右键某些选项变成不可用
                {
                    sBookType = tnClick.ToolTipText;
                    cmsAdd.Enabled = false;
                    cmsDel.Enabled = false;
                    cmsDown.Enabled = false;
                    cmsNew.Enabled = true;
                    cmsSearch.Enabled = false;//搜索本书 
                }
            }
        }

        private void cmsDel_Click(object sender, EventArgs e)
        {
            if (tsbBook.BackColor == System.Drawing.SystemColors.ControlLightLight)//如果当前在书库页面，完全删除
            {
                if (MessageBox.Show("确定删除么（经典小说与我的书架）？", "删除书籍", MessageBoxButtons.YesNo).ToString() != "Yes")
                    return;
                //如果取消了，退出
                deleteBook(true);
            }
            else
            {
                deleteBook(false);
            }
        }
        private void deleteBook(bool bDelteComplete)
        {
            int iRow;
            try
            {
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                if (bDelteComplete)
                {
                    odCommand.CommandText = "delete from MyBook where BookName='" + sBookName + "'";
                    iRow = odCommand.ExecuteNonQuery();
                    odCommand.CommandText = "delete from Book where BookName='" + sBookName + "'";
                    iRow += odCommand.ExecuteNonQuery();
                }
                else
                {
                    odCommand.CommandText = "delete from MyBook where BookName='" + sBookName + "'";
                    iRow = odCommand.ExecuteNonQuery();
                }
                //建立读取
                //在书库列表里面删除的情况
                TreeNode tnParent = tnBook.Parent;
                
                if (bDelteComplete==true)
                {   
                    //首先删除当前的树
                    tnBook.Remove();
                    tnParent.Text = tnParent.ToolTipText + "(" + tnParent.Nodes.Count + ")";
                    //然后删除书架的书，因为当前不在书架页面
                    //如果我的书架中有这个类型（!=-1)
                    int iIndex=tvMyBook.Nodes.IndexOfKey(tnParent.Name) ; //在书架中的序号
                   
                    if (iIndex!= -1)
                    {
                        tnParent = tvMyBook.Nodes[iIndex];  //取得这个结点。
                        iIndex = tnParent.Nodes.IndexOfKey(sBookName);//如果能够找到这本书,移除
                        if (iIndex != -1)
                        {
                            tnParent.Nodes[iIndex].Remove();
                            if (tnParent.Nodes.Count == 0)
                                tnParent.Remove();
                            else
                                tnParent.Text = tnParent.ToolTipText + "(" + tnParent.Nodes.Count +")";                          
                        }
                    }
                }
                 //在我的书架里面删除的情况
                else
                {
                    tnBook.Remove();
                    //如果该书籍类型已经不存在书籍了，那么分类也不要了
                    if (tnParent.Nodes.Count == 0)
                        tnParent.Remove();
                }
                if(iRow==0)
                {
                    MessageBox.Show("书籍删除失败");
                }
            }
            catch (Exception ex)
            {
                 string info = ex.Message + ex.Data;
                 MessageBox.Show("deleteBook"+ex.Message+ex.Data);
            }
            finally
            {
                odcConnection.Close();
            }
        }
        private void tsbNew_Click(object sender, EventArgs e)
        {
            if (tsbMyBook.BackColor == SystemColors.ControlLightLight)//根据背景颜色来判断，是在哪一个列表里面
            {
                addform addbook = new addform(odcConnection, sBookType, 1,this,contextMenuStrip);
            }
            else if ((tsbBook.BackColor == SystemColors.ControlLightLight))
            {
                addform addbook = new addform(odcConnection, sBookType, 0,this,contextMenuStrip);
            }
            else
            {
                MessageBox.Show("请指定添加到哪个书库！");
            }

        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            int icount = 0;//保存设置
            HtmlElement element = myBrowser.Document.GetElementById("scrollspeed");
            if (element != null)
            {
                sScrollSpeed = element.GetAttribute("value");
                icount++;
            }

            element = myBrowser.Document.GetElementById("backcolor");
            if (element != null)
            {
                sBackColor = element.GetAttribute("value");
                icount++;
            }

            element = myBrowser.Document.GetElementById("backsize");
            if (element != null)
            {
                sFontSize = element.GetAttribute("value");
                icount++;
            }
            element = myBrowser.Document.GetElementById("txtcolor");
            if (element != null)
            {
                sTextColor = element.GetAttribute("value");
                icount++;
            }

            if (icount < 4)
            {
                MessageBox.Show("保存设置错误，网页中没有相应元素。");
                return;
            }
            else
            {
                int iRow;
                try
                {
                    odcConnection.Open();
                    OleDbCommand odCommand = odcConnection.CreateCommand();
                    odCommand.CommandText = " update ReadSet set ScrollSpeed ='"+sScrollSpeed+"',BackColor='"+sBackColor+"',FontSize='"+sFontSize+"',TextColor='"+sTextColor+"'";
                    //建立读取
                    iRow = odCommand.ExecuteNonQuery();
                    odcConnection.Close();

                    asReadSet.Clear();
                    asReadSet.Add(sScrollSpeed);
                    asReadSet.Add(sBackColor);
                    asReadSet.Add(sFontSize);
                    asReadSet.Add(sTextColor);

                    if (iRow == 0)
                        MessageBox.Show("保存设置失败");
                }
                catch (Exception ex)
                {
                    string info = ex.Message + ex.Data;
                    // MessageBox.Show("保存设置:" + ex.Message + ex.Data);
                    workStatus.Text = "保存设置出错！";
                }
                finally
                {
                    odcConnection.Close();
                }
            }
        }

        private void tsbSearch_Click(object sender, EventArgs e)
        {
            myBrowser.Navigate(SearchURL);
        }

        private void tsbRank_Click(object sender, EventArgs e)
        {
            string sRegRank = "";
            string sRankName = "";
            string sRankURL = "";
            string sURLFormat = "";
            string sHtml = "";

            string sRankBookName;
            string sRankBookURL;

            try
            {
                //保存信息
                if (tsbMyBook.BackColor == System.Drawing.SystemColors.ControlLightLight)
                    cloneTreeView(treeView, tvMyBook);
                if (tsbBook.BackColor == System.Drawing.SystemColors.ControlLightLight)
                    cloneTreeView(treeView, tvBook);
                //设置情况
                tsbBook.BackColor = System.Drawing.SystemColors.Control;
                tsbMyBook.BackColor = System.Drawing.SystemColors.Control;
                tsbRank.BackColor = System.Drawing.SystemColors.ControlLightLight;

                OleDbDataReader odrReader;
                odcConnectionConfig.Open();
                //如果已经存在了，就不再查询，直接复制一份后返回
                if (tvRank.Nodes.Count > 0)
                {
                    cloneTreeView(tvRank, treeView);
                    treeView.ExpandAll();
                    return;
                }

                //首先根据名称查询到正则表达式信息以及所在的网页URL，名称。
                workStatus.Text = "正在查询请稍候...";
                OleDbCommand odCommand = odcConnectionConfig.CreateCommand();
                odCommand.CommandText = "select rtrim(RankName) as RankName, rtrim(RankURL) as RankURL,rtrim(RegRank) as RegRank,rtrim(URLFormat) as URLFormat FROM Rank ";
                //读取
                odrReader = odCommand.ExecuteReader();

                //查询并显示数据
                TreeNode tnNew = null;
                TreeNode tnNewSon = null;
                treeView.Nodes.Clear();
                while (odrReader.Read())
                {
                    sRankName = odrReader["RankName"].ToString(); ;
                    sRankURL = odrReader["RankURL"].ToString();
                    sRegRank = odrReader["RegRank"].ToString();
                    sURLFormat = odrReader["URLFormat"].ToString();
                    //增加XXX排行节点
                    tnNew = treeView.Nodes.Add(sRankName);
                    tnNew.SelectedImageIndex = 0;
                    tnNew.ImageIndex = 0;
                    sHtml = getFileFromWeb.getHtml(sRankURL, "");
                    Match match = Regex.Match(sHtml, sRegRank, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    int iBookCount = 0;
                    while (match.Success && iBookCount < 10)
                    {
                        sRankBookURL = match.Groups[1].Value;
                        sRankBookURL = sURLFormat.Replace("<#URL>", sRankBookURL);
                        sRankBookName = match.Groups[2].Value;
                        tnNewSon = tnNew.Nodes.Add(sRankBookURL);
                        tnNewSon.ToolTipText = sRankBookName;
                        tnNewSon.Text = sRankBookName;
                        tnNewSon.Tag = sRankBookURL;
                        tnNewSon.ContextMenuStrip = contextMenuStrip;
                        tnNewSon.SelectedImageIndex = 1;
                        tnNewSon.ImageIndex = 2;
                        match = match.NextMatch();
                        iBookCount++;
                    }

                }
                //关闭连接
                odrReader.Close();
                treeView.ExpandAll();
                workStatus.Text = "就绪";

                if (splitContainer.SplitterDistance == 0)
                    tsbFullScreen_Click(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                // MessageBox.Show("获得小说排行出错" + ex.Message + ex.Data);
                workStatus.Text = "获得小说排行出错！";
            }
            finally
            {
                workStatus.Text = "就绪";
                odcConnectionConfig.Close();
            }
        }

        private void stbUpdate_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("update.exe");

            Application.Exit();

            Environment.Exit(Environment.ExitCode);

           
        }

        private void tsbSelect_Click(object sender, EventArgs e)
        {
            if (bSelect == false)
            {
                tsbSelect.BackColor = SystemColors.ControlLightLight;
                this.myBrowser.Navigating += new WebBrowserNavigatingEventHandler(this.myBrowser_Navigating);
                this.myBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.myBrowser_DocumentCompleted);
                
                selectType.Text = "模式：过滤";
                bSelect = true;

            }
            else
            {
                tsbSelect.BackColor = SystemColors.Control;
                this.myBrowser.Navigating -= new WebBrowserNavigatingEventHandler(this.myBrowser_Navigating);
                this.myBrowser.DocumentCompleted -= new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.myBrowser_DocumentCompleted);

                selectType.Text = "模式：直接";
                bSelect = false;
            }
        }

        private void tsbVoice_Click(object sender, EventArgs e)
        {
            if (bVoice == true)
            {
                bVoice = false;
                tsbVoice.BackColor = SystemColors.Control;
                sp.Skip("Sentence",9999);
                
            }
            else
            {
                bVoice = true;
                tsbVoice.BackColor = SystemColors.ControlLightLight;
                if (sCurrentFile != getFileFromWeb.changeToSaveFormat(sBookURLFoot))
                    readTxtFile(Application.StartupPath + "\\book\\" + sBookName + "\\" + sSaveFileName);
            }          
        }

        private void tsbAbout_Click(object sender, EventArgs e)
        {
            aboutSoft.ShowDialog();
        }
    }
    /// <summary>
    ///将下载的网页内容进行处理
    /// </summary>
    public class buildHtml
    {
       /// <summary>
       /// 建立索引页面
       /// </summary>
       /// <param name="sHtml">网页文本内容</param>
       /// <param name="sBookHead">书目的头部分比如http://www.lieshu.net/book/1/</param>
       /// <param name="sSaveDir">保存的目录</param>
       /// <param name="sSaveFileName">保存的文件名称</param>
       /// <param name="asBookRegP">书籍内容筛选的正则表达式</param>
       /// <param name="asBookSet">设置列表</param>
       /// <returns>返回一个章节列表字符串</returns>
       /// 
        public static ArrayList makeIndex(string sHtml,string sBookHead, string sSaveDir,string sSaveFileName, ArrayList asBookRegP,ArrayList asBookSet)
        //sHtml为传入的网页内容,
        {
            string sURL, sTitle;//超链接以及显示的文字
            string sReplaceTitle="";
            string sReplaceContent="";//用于替代模板中列表部分的文字

            string sRegBookName="";
            string sRegChapterList="";
            ArrayList asChapterList = new ArrayList();

            string sScrollSpeed = "5";//默认滚动速度
            string sBackColor = "#E7F4FE";//默认背景颜色
            string sFontSize = "12";
            string sTextColor = "#000000";
            sScrollSpeed = asBookSet[0].ToString();
            sBackColor = asBookSet[1].ToString();
            sFontSize = asBookSet[2].ToString();
            sTextColor = asBookSet[3].ToString();

            try
            {
                //字符串数组本地化
                sRegBookName = asBookRegP[0].ToString();
                sRegChapterList = asBookRegP[1].ToString();

                //获得标题部分
                Match match = Regex.Match(sHtml, sRegBookName, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                sReplaceTitle = match.Groups[1].Value;

                //获得所有的符合条件的超链接
                match = Regex.Match(sHtml, sRegChapterList, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                int iOrder = 0; //计数
                while (match.Success)
                {
                    sURL = match.Groups[1].Value;
                    sTitle = match.Groups[2].Value.Trim();
                    sTitle = Regex.Replace(sTitle, "[\\s]*\\s", " ", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                //转化一下。如果sURL是以全地址的。则比较和主页的前面部分是不是一样
                //比如 http://www.lieshu.net/book/1/2.htm这样的地址，而目录是http://www.lieshu.net/book/1/index.htm
                //两个都有http://www.lieshu.net/book/1/这个头，那就把ChapterList里面只取后面部分。

                    if (sURL.IndexOf("http://") == 0|| sURL.IndexOf("/") == 0)
                    {
                        string sTempURL = getFileFromWeb.getPathFromURL(sURL);
                        sTempURL = sTempURL.Substring(1, sTempURL.Length - 1);
                        if(getFileFromWeb.getHostFromURL(sBookHead) + sTempURL == sBookHead||getFileFromWeb.getPathFromURL(sURL) == sBookHead)
                            sURL = getFileFromWeb.getNameFromURL(sURL);
                    }
                    //用htm方式存储
                    sURL = getFileFromWeb.changeToSaveFormat(sURL);
                    asChapterList.Add(sURL);
                    //<div><a href='' title=''></a></div>
                    sReplaceContent += "<div><a href='" + sURL + "' title='" + sTitle + "'>" + sTitle + "</a></div>\r\n";//还是保存相对地址吧。

                    match = match.NextMatch();
                    iOrder++;
                }

                //读出主页模板文件 
                StreamReader sr = new StreamReader(Application.StartupPath + "\\model\\ModelIndex.htm");
                string sModel = sr.ReadToEnd();
                sr.Close();
                //替换内容
                sModel = sModel.Replace("<#TITLE>", sReplaceTitle);
                sModel = sModel.Replace("<#DATA>", sReplaceContent);

                sModel = sModel.Replace("<#SPEED>", sScrollSpeed);
                sModel = sModel.Replace("<#BGCOLOR>", sBackColor);
                sModel = sModel.Replace("<#FONTSIZE>", sFontSize);
                sModel = sModel.Replace("<#TEXTCOLOR>", sTextColor);

                //检测文件夹是否存在
                if (!Directory.Exists(sSaveDir))
                    Directory.CreateDirectory(sSaveDir);
                //写入保存文件
                StreamWriter sw = new StreamWriter(sSaveDir + sSaveFileName, false, Encoding.UTF8);
                sw.WriteLine(sModel);
                sw.Close();
            }
            catch (Exception ex
                )
            {
                string info = ex.Message + ex.Data;
                // MessageBox.Show("makeIndex"+e.Message+e.Data);
            }
            //返回章节列表
            return asChapterList;


        }
        /// <summary>
        /// 将内容以规定的方式筛选提取出内容，然后保存到指定目录，有文件则覆盖。
        /// </summary>
        /// <param name="sHtml">传入的文本内容</param>
        /// <param name="sBookName">书籍名称</param>
        /// <param name="sSaveDir">保存目录</param>
        /// <param name="sSaveFileName">保存的文件名称,saveformated</param>
        /// <param name="sBookHead">目录地址的头部分http://www.lieshu.net/1234/</param>
        /// <param name="sBookIndex">网页目录名称，用于导航</param>
        /// <param name="asBookReg">传入的书籍正则表达式</param>
        /// <param name="asChapterListP">章节地址列表，用于导航</param>
        /// <param name="asBookSet">书籍阅读的设置，滚动速度、背景亚瑟、字体大小、文字颜色</param>
        public static  string  makeContent(string sHtml, string sBookName, string sSaveDir, string sSaveFileName,string sBookHead, string sBookIndex, ArrayList asBookReg, ArrayList asChapterListP,ArrayList asBookSet)
        //生成内容页
        {
            try
            {
                string sRegChapterTitle = "";//标题正则表达式
                string sRegChapterContent = "";//内容正则表达式
                string sRegContentExcept = "";//需要排除的内容

                string sReplaceTitle = "";//新的标题
                string sReplaceContent = "";//新的内容
                
                string sLastChapterURL = "";//上一章节
                string sNextChapterURL = "";//下一章节
                string sEndChapterURL = "";//末尾章节

                string sOldImageURL = "";//老的图片地址
                string sContentHost = getFileFromWeb.getHostFromURL(sBookHead);
                sBookIndex = getFileFromWeb.changeToSaveFormat(sBookIndex);

                string sScrollSpeed = "5";//默认滚动速度
                string sBackColor = "#E7F4FE";//默认背景颜色
                string sFontSize = "12";
                string sTextColor = "#000000";
                sScrollSpeed = asBookSet[0].ToString();
                sBackColor = asBookSet[1].ToString();
                sFontSize = asBookSet[2].ToString();
                sTextColor = asBookSet[3].ToString();
                

                //将两个传递过来的ArrayList本地化，因为是地址，在多线程处理中，如果在其它函数中修改了内容，这里也变化掉了。
                ArrayList asChapterList = new ArrayList();
                asChapterList.Clear();
                for (int i = 0; i < asChapterListP.Count; i++)
                    asChapterList.Add(asChapterListP[i].ToString());

                sRegChapterTitle = asBookReg[2].ToString();
                sRegChapterContent = asBookReg[3].ToString();
                sRegContentExcept = asBookReg[4].ToString();



                //color='#CC3300'>第五章 虎落平阳被狗欺(上)</font></span>
                Match match = Regex.Match(sHtml, sRegChapterTitle, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                if (match.Success)
                {
                    sReplaceTitle = match.Groups[1].Value;
                }
                //130%;">然后是内容</span><br>
                
                match = Regex.Match(sHtml, sRegChapterContent, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                if (match.Success)
                    sReplaceContent = match.Groups[1].Value;
                else//如果没有成功，就以BODY再来一次
                {
                    match = Regex.Match(sHtml, "<[^<>]*BODY[^<>]*>([^\\v]*)<[^<>]*/BODY>[^<>]*", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    sReplaceContent = match.Groups[1].Value;//在不匹配就没办法了。
                }
                //图片的绝对地址变为全地址。
               // <img src="/DownFiles/Book/11/11423/2009/3/9/20090309111857282.gif" align="center" border=0>
                match = Regex.Match(sHtml, "<IMG[^<>s]*src=\"(/[^\"'>]*)\"[^>]*>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                //清理一下截取出来的内容
                while (match.Success)
                {
                    sOldImageURL = match.Groups[1].Value;
                     sReplaceContent=sReplaceContent.Replace(sOldImageURL, sContentHost + sOldImageURL);

                    match = match.NextMatch();
                }


                sReplaceContent = clearHTML(sReplaceContent,sRegContentExcept);


                sEndChapterURL = asChapterList[asChapterList.Count - 1].ToString();
                //遍历，以找到当前章节的上一个和下一个网页
                for (int i = 0; i < asChapterList.Count; i++)
                {
                    if (asChapterList[i].ToString() == sSaveFileName)
                    {
                        if (i == 0)
                        {
                            sLastChapterURL = sBookIndex;
                            sNextChapterURL = asChapterList[i + 1].ToString();

                        }
                        else if (i == asChapterList.Count - 1)
                        {
                            sLastChapterURL = asChapterList[i - 1].ToString();
                            sNextChapterURL = sBookIndex;
                        }
                        else
                        {
                            sLastChapterURL = asChapterList[i - 1].ToString();
                            sNextChapterURL = asChapterList[i + 1].ToString();
                        }
                    }
                }
                //打开模板文件。
                StreamReader sr = new StreamReader(Application.StartupPath + "\\model\\ModelChapter.htm");
                string sModel = sr.ReadToEnd();
                sr.Close();

                //开始替换
                sModel = sModel.Replace("<#TITLE>", sReplaceTitle);
                sModel = sModel.Replace("<#CONTENT>", sReplaceContent);

                sModel = sModel.Replace("<#BOOK>", sBookName);
                sModel = sModel.Replace("<#LAST>", sLastChapterURL);
                sModel = sModel.Replace("<#NEXT>", sNextChapterURL);
                sModel = sModel.Replace("<#END>", sEndChapterURL);
                sModel = sModel.Replace("<#INDEX>", sBookIndex);

                sModel = sModel.Replace("<#SPEED>", sScrollSpeed);
                sModel = sModel.Replace("<#BGCOLOR>", sBackColor);
                sModel = sModel.Replace("<#FONTSIZE>", sFontSize);
                sModel = sModel.Replace("<#TEXTCOLOR>", sTextColor);

                if (!Directory.Exists(sSaveDir))
                    Directory.CreateDirectory(sSaveDir);

                StreamWriter sw = new StreamWriter(sSaveDir + sSaveFileName, false, Encoding.UTF8);
                sw.WriteLine(sModel);
                sw.Close();
                return sReplaceTitle;
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                return "";
                // MessageBox.Show("makeContent"+e.Message+e.Data);
            }

        }
        /// <summary>
        /// 清理HTML中的Table、tr、td标签
        ///  </summary>
        /// <param name="sHtml">要清理的网页内容</param>
        /// <param name="sRegContentExcept">需要排除的字符串</param>
        /// <returns>清理后的网页内容</returns>
        public static string clearHTML(string sHtml,string sRegContentExcept)
        {

            //需要去掉的内容
            if (sRegContentExcept != "")
                sHtml = Regex.Replace(sHtml, sRegContentExcept, "", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            sHtml = Regex.Replace(sHtml, "<[ /]*CENTER[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*UL[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*LI[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*SPAN[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ ]*HR[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<!--[^\\f]*?-->", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*TABLE[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*FONT[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*TR[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*TD[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*BR[^<>]*>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*STRONG>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "&[a-z0-9#]{3,4};", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[^<>]*DIV[^<>]*>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[^<>]*SCRIPT[^<>]*>[^<>]*<[^<>]*/SCRIPT[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[^<>]*FIELDSET[^<>]*>[^\v]*<[^<>]*/FIELDSET[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[^<>]*IFRAM[^<>]*>[^<>]*<[^<>]*/IFRAM[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[^<>]*SCRIPT[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);//去掉还没有配对的script
            sHtml = Regex.Replace(sHtml, "<[^<>]*A[^<>]*>[^<>]*<[^<>]*/A[^<>]*>", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);//所有的<A></A>
            sHtml = Regex.Replace(sHtml, "<[ /]*A[^<>]*>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            sHtml = Regex.Replace(sHtml, "<[ /]*P[^<>]*>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);//不同的段落格式装换为一种
            sHtml = Regex.Replace(sHtml, "[\\f\\n\\t\\r\\v]*", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);//去掉所有空白符号
            sHtml = Regex.Replace(sHtml, "<P>[<P> ]*<P>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);//多个连续的仅放一个



            return sHtml;
        }
    }
/// <summary>
/// 书籍操作类
/// </summary>
    public class bookOperate
    {

        mainForm mainform; //传入的表。
        string sBookName;
        string sBookHead;
        string sBookIndex;
        string sSaveDir;
        ArrayList asBookReg;
        ArrayList asChapterList;
        ArrayList asBookSet;
        int iPacketType;
        delegate void SetTextCallback(string status, string downtext);//设置委托
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="mainformP">窗口</param>
        /// <param name="sBookNameP">书籍名称</param>
        /// <param name="sBookHeadP">书籍网址的前面部分</param>
        /// <param name="sBookIndexP">书籍目录文件名</param>
        /// <param name="sSaveDirP">保存目录</param>
        /// <param name="asRookRegP">书籍的正则表达式</param>
        /// <param name="asChapterListP">章节列表</param>
        /// <param name="asBookSetP">要保存到下载文件中的阅读设置</param>
        public bookOperate(mainForm mainformP,string sBookNameP,string sBookHeadP,string sBookIndexP,string sSaveDirP,ArrayList asRookRegP,ArrayList asChapterListP,ArrayList asBookSetP,int ipackettype)//构造函数中初始化
        {
            mainform=mainformP;
            sBookName = sBookNameP;
            sBookHead=sBookHeadP;
            sBookIndex = sBookIndexP;
            sSaveDir = sSaveDirP;
            iPacketType = ipackettype;
            asBookReg = new ArrayList();
            asBookReg.Clear();
            for (int i = 0; i < asRookRegP.Count; i++)
                asBookReg.Add(asRookRegP[i]);

            asChapterList = new ArrayList();
            asChapterList.Clear();
            for (int i = 0; i < asChapterListP.Count; i++)
                asChapterList.Add(asChapterListP[i]);

            asBookSet = new ArrayList();
            asBookSet.Clear();

            for (int i = 0; i < asBookSetP.Count; i++)
                asBookSet.Add(asBookSetP[i]);

        }

        /// <summary>
        /// 读取这本书的正则表达信息
        /// </summary>
        /// <param name="sSiteHost">书所在的网站主机部分，如http://www.lieshu.net/</param>
        /// <param name="odcConnection">连接</param>
        /// <returns>以字符串数组的方式返回信息</returns>
        public static ArrayList getBookReg(string sSiteHost,OleDbConnection odcConnection)
        {

            OleDbDataReader odrReader;
            ArrayList asRegList = new ArrayList();
            try
            {         
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "select rtrim(RegBookName) as RegBook,rtrim(RegChapterList) as RegChapter,rtrim(RegChapterTitle) as  RegTitle,rtrim(RegChapterContent) as  RegContent ,rtrim(RegContentExcept) as RegExcept,rtrim([DefaultIndex]) as [DefaultIndex] from Site where SiteIndex='http://default/' or SiteIndex = '" + sSiteHost + "' order by SiteID desc";
                
                odrReader = odCommand.ExecuteReader();
                odrReader.Read();
                //如果读取成功
                asRegList.Add(odrReader["RegBook"].ToString());
                asRegList.Add(odrReader["RegChapter"].ToString());
                asRegList.Add(odrReader["RegTitle"].ToString());
                asRegList.Add(odrReader["RegContent"].ToString());
                asRegList.Add(odrReader["RegExcept"].ToString());
                asRegList.Add(odrReader["DefaultIndex"].ToString());


                //关闭连接
                odrReader.Close();
                return asRegList;
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                // MessageBox.Show("getBookReg"+ex.Message+ex.Data);
                return null;
            }
            finally
            {

                odcConnection.Close();
            }

        }
        /// <summary>
        /// 设置工作状态
        /// </summary>
        /// <param name="status"></param>
        /// <param name="downtext"></param>
        public void setWorkStatus(string status, string downtext)
        {
            //来设置工作状态，解决多线程下不能设置问题
            if (mainform.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(setWorkStatus);
                mainform.Invoke(d, new object[] { status, downtext });
                
            }
            else
            {
                mainform.workStatus.Text = status;
                if (downtext != "")
                    mainform.tsbDown.Text = downtext;
            }
            
        }
        /// <summary>
        /// 下载并打包
        /// </summary>
        public void downAndPacket()
        {
            downAllChapter();
            packetBook();
        }
         /// <summary>
         /// 下载所有章节
         /// </summary>
        public void downAllChapter()
        {
            string sloadingFile;

            int itotalCount = asChapterList.Count;
            for (int i = 0; i < itotalCount; i++)
            {
                sloadingFile = asChapterList[i].ToString();
                if (File.Exists(sSaveDir + sloadingFile))
                {//如果文件存在，则跳过不下载
                    //
                    continue;
                }
                else
                {//下载文件
                    sloadingFile = getFileFromWeb.changeToVisitFormat(sloadingFile);
                    string sHtml = getFileFromWeb.getHtml( sBookHead+ sloadingFile, "");
                    buildHtml.makeContent(sHtml,sBookName,sSaveDir,getFileFromWeb.changeToSaveFormat(sloadingFile),sBookHead,sBookIndex,asBookReg,asChapterList,asBookSet);
                }
                Thread.Sleep(100);
                setWorkStatus("《" + sBookName + "》下载进度:共" + itotalCount + "章，第" + i + "章完成", "");
            }
            setWorkStatus("《" + sBookName + "》下载进度:全部完成。", "");
        }
        //打包成Txt格式
        private void packetAsTxt()
        {
            //打包程序，输入为书籍名称
            //1、首先确认所有页面都已经下载完成
            //2、检查主页是否存在
            //3、开始打包
            string sFilePath;//书籍路径
            string sSavePath;//书籍保存路径
            string sBookIndexURL = getFileFromWeb.changeToSaveFormat(sBookIndex);//目录地址
            string sHtml;//取出的内容
            string sChapterFileName;//当前处理的章节地址
            string sChapterTitle;//当前处理的章节标题
            string sResult;
            //附加的软件信息
            string sAddtion = "\r\n\r\n制作：清风小说浏览器-猎书网\r\n\r\n";
            string sNoChapter = "\r\n本章节不存在\r\n";
            int iOrder;
            Match matchList;
            Match matchContent;
            try
            {
                sFilePath =sSaveDir;
                sSavePath = Application.StartupPath + "\\chm\\";  //保存txt的目录
                if (!File.Exists(sFilePath + sBookIndexURL))//如果文件不存在就直接退出了。
                {
                    MessageBox.Show("《" + sBookName + "》的目录文件不存在");
                    setWorkStatus("《" + sBookName + "》下载出现错误", "下载");
                    return;
                }
                //读取文件
                StreamReader sr = new StreamReader(sFilePath + sBookIndexURL);
                sHtml = sr.ReadToEnd();
                sr.Close();

                //获得所有超链接

                matchList = Regex.Match(sHtml, "<A HREF='([^\"']*)'[^>]*>([^<>]*)</A>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                sHtml = "";
                sResult = "";
                iOrder = 0;
                while (matchList.Success)
                {
                    sChapterFileName = matchList.Groups[1].Value;
                    sChapterTitle = matchList.Groups[2].Value;
                    if (!File.Exists(sFilePath + sChapterFileName))
                    {
                        setWorkStatus("章节：" + sChapterTitle + "不存在，忽略", "");
                        sResult += sChapterTitle + sNoChapter + sAddtion;
                        matchList = matchList.NextMatch();
                        continue;//跳出进入下一个循环
                    }
                    //读取文件
                    setWorkStatus("《" + sBookName + "》打包进度:正在处理" + sChapterTitle, "");
                    sr = new StreamReader(sFilePath + sChapterFileName);
                    sHtml = sr.ReadToEnd();
                    sr.Close();
                    //获得内容部分
                    matchContent = Regex.Match(sHtml, "<!--content start-->([^\v]*)<!--content end-->", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    //替换所有的<p>为回车换行
                    sHtml = matchContent.Groups[1].Value;
                    sHtml = Regex.Replace(sHtml, " ", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    sHtml = Regex.Replace(sHtml, "　", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    sHtml = Regex.Replace(sHtml, "<P>[<P> ]*<P>", "<P>", RegexOptions.IgnoreCase | RegexOptions.Multiline);//多个连续的仅放一个
                    sHtml = Regex.Replace(sHtml, "<P>", "\r\n　　", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    sHtml = sHtml.Trim();
                    sResult += sChapterTitle + "\r\n　　" + sHtml + sAddtion;
                    iOrder++;

                    matchList = matchList.NextMatch();
                }
                //最后写入一个文件
                if (!Directory.Exists(sSavePath))
                    Directory.CreateDirectory(sSavePath);

                StreamWriter sw = new StreamWriter(sSavePath + sBookName + ".txt", false, Encoding.UTF8);//以UTF8编码存储
                sw.WriteLine(sResult);
                sw.Close();
                MessageBox.Show("保存在chm文件夹下，文件名：" + sBookName + ".txt", "打包完成");
                setWorkStatus("《" + sBookName + "》打包进度:全部完成", "下载");
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                //MessageBox.Show("packetBook"+ex.Message+ex.Message+ex.Data);
            }
        }
        //只需要知道书籍名称和目录文件就可以了。或许可以用重载，简化一下。
        private void packetAsChm()
        {
            string sHtml;//用于保存读取的网页内容。
            string sChapterURL;//某个章节的地址
            string sChapterTitle;//章节标题
            string sModelPath = Application.StartupPath + "\\model\\";//模板所在的位置
            string sSavePath = Application.StartupPath + "\\chm\\";  //保存txt的目录
            string sBookIndexURL = getFileFromWeb.changeToSaveFormat(sBookIndex);

            string sReplaceFileList;//替换book.hhp中,<#FILELIST> 部分
            string sReplaceContentList;//替换contents.hhc中<#CONTENTLIST> 部分

            //单个内容条目，替换掉其中的章节名称和地址然后组合
            string sConentList = "<LI> <OBJECT type=\"text/sitemap\">\r\n";
            sConentList +="<param name=\"Name\" value=\"<#CHAPTERTITLE>\">\r\n";
            sConentList += "<param name=\"Local\" value=\"<#CHAPTERURL>\">\r\n </OBJECT>\r\n";

            string sTemp;//临时用的字符串

            Match matchList;//符合条件的章节列表
            try
            {
                if (!File.Exists(sSaveDir + sBookIndexURL))//如果文件不存在就直接退出了。
                {
                    MessageBox.Show("《" + sBookName + "》的目录文件不存在,打包终止");
                    setWorkStatus("", "下载");
                    return;
                }

                StreamReader sr = new StreamReader(sSaveDir + sBookIndexURL);
                sHtml = sr.ReadToEnd();
                sr.Close();
                //获得所有的超链接
                matchList = Regex.Match(sHtml, "<A HREF='([^\"']*)'[^>]*>([^<>]*)</A>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                sReplaceContentList = "";
                sReplaceFileList = "";
                while (matchList.Success)
                {
                    //读取章节名称和地址
                    sChapterURL = "book\\" + sBookName + "\\" + matchList.Groups[1].Value;
                    sChapterTitle = matchList.Groups[2].Value;
                    //替换并连接起来
                    sReplaceFileList += sChapterURL + "\r\n";
                    sTemp = sConentList;
                    sTemp = sTemp.Replace("<#CHAPTERTITLE>", sChapterTitle);
                    sTemp = sTemp.Replace("<#CHAPTERURL>", sChapterURL);
                    sReplaceContentList += sTemp;

                    matchList = matchList.NextMatch();
                }

                //读取并替换book.hhp文件内容
                if (!File.Exists(sModelPath + "book.hhp"))//如果文件不存在就直接退出了。
                {
                    MessageBox.Show("模板文件book.hhp不存在,打包终止");
                    setWorkStatus("", "下载");
                    return;
                }
                //读取book.hhp模板文件,并且替换其中的<#BOOKNAME>,<#INDEXURL>,<#FILELIST>,然后保存为temp.hhp
                sr = new StreamReader(sModelPath + "book.hhp");
                sHtml = sr.ReadToEnd();
                sr.Close();

                sHtml = sHtml.Replace("<#BOOKNAME>", sBookName);
                sHtml = sHtml.Replace("<#INDEXURL>", sBookIndexURL);
                sHtml = sHtml.Replace("<#FILELIST>", sReplaceFileList);


                StreamWriter sw = new StreamWriter("temp.hhp", false, Encoding.Default);//以UTF8编码存储
                sw.WriteLine(sHtml);
                sw.Close();

                //读取并替换book.hhc内容,保存为temp.hhc
                if (!File.Exists(sModelPath + "book.hhc"))//如果文件不存在就直接退出了。
                {
                    MessageBox.Show("模板文件book.hhc不存在,打包终止");
                    setWorkStatus("", "下载");
                    return;
                }
                //读取book.hhp模板文件,并且替换其中的<#BOOKNAME>,<#INDEXURL>,<#FILELIST>,然后保存为temp.hhp
                sr = new StreamReader(sModelPath + "book.hhc");
                sHtml = sr.ReadToEnd();
                sr.Close();
                //替换
                sHtml=sHtml.Replace("<#CONTENTLIST>",sReplaceContentList);
                //保存
                sw = new StreamWriter("temp.hhc", false, Encoding.Default);//以UTF8编码存储
                sw.WriteLine(sHtml);
                sw.Close();

                //开始打包
                setWorkStatus("正在打包...", "");
                //设置参数
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
                //执行的文件
                info.FileName = "HHC.exe";
                //后续参数
                info.Arguments = "temp.hhp";
                //不显示界面
                info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //定义进程，并开始
                System.Diagnostics.Process proc;
                proc = System.Diagnostics.Process.Start(info);

                //每隔1秒循环测试，是否已经打包完成
                while(!proc.HasExited)
                    Thread.Sleep(1000);

                //删除temp.hhp和temp.hhc连个临时文件
                File.Delete("temp.hhc");
                File.Delete("temp.hhp");
                MessageBox.Show("保存在chm文件夹下，文件名："+sBookName+".chm","打包完成");
                setWorkStatus("完成打包", "下载");
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                //MessageBox.Show("packetBook"+ex.Message+ex.Message+ex.Data);
            }

        }
/// <summary>
/// 打包书籍,类型为1:Txt，2:Chm格式
/// </summary>
        public  void packetBook()
        {
            setWorkStatus("开始打包...", "停止");
            switch (iPacketType)
            {
                //1为Txt格式
                case 1:
                    packetAsTxt();
                    break;
                case 2:
                    packetAsChm();
                    break;
                default: break;//其它情况,不处理
            }

        }

    }
    ///<summary>
    ///从网上获得文件等,仅定义静态方法  
    ///</summary>
    public class getFileFromWeb
    {
        
        /// <summary>
        /// 使用WebClient方法，从指定地址获得文本
        /// </summary>
        /// <param name="url">string</param>
        /// <param name="charSet">string</param>
        /// <returns>用\n符号表示网络错误</returns>
        public static string getHtml(string url, string charSet)
        //url是要访问的地址，charSet是目标网页的编码，如果传入的是null或者""，那就自动分析网页的编码 
        {
            try
            {
                WebClient myWebClient = new WebClient();

                myWebClient.Credentials = CredentialCache.DefaultCredentials;
                //获得数据
                byte[] myDataBuffer = myWebClient.DownloadData(url);
                string strWebData = Encoding.Default.GetString(myDataBuffer);

                //获取网页字符编码描述信息 
                Match charSetMatch = Regex.Match(strWebData, "<meta([^<]*)charset=([^<]*)\"", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                string webCharSet = charSetMatch.Groups[2].Value;
                if (charSet == null || charSet == "")
                    charSet = webCharSet;

                if (charSet != null && charSet != "" && Encoding.GetEncoding(charSet) != Encoding.Default)
                    strWebData = Encoding.GetEncoding(charSet).GetString(myDataBuffer);
                return strWebData;
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                // MessageBox.Show("getHtml"+ex.Message+ex.Message+ex.Data);
                return null;
            }
        }

        ///<summary>
        ///Web Client 方法，仅适用于小图片，如果在相同目录下存在相同文件，则删除
        /// </summary>
        /// <param name="fileName">loadfile.htm</param>
        /// <param name="url">http://www.lieshu.net/index.htm?fang=good</param>
        /// <param name="localPath">E:\\Fang\</param>
        public static void getFile(string url, string localPath,string fileName)
        {
            try
            {
                WebClient myWebClient = new WebClient();
                if (fileName == "" || fileName == null)
                    fileName = getNameFromURL(url);
                if (File.Exists(localPath + fileName))
                {
                    File.Delete(localPath + fileName);
                }
                if (Directory.Exists(localPath) == false)
                {
                    Directory.CreateDirectory(localPath);
                }
                myWebClient.DownloadFile(url, localPath + fileName);
            }
            catch (WebException ex)
            {
                string info = ex.Message + ex.Data;
                //MessageBox.Show(("getFile"+e.Message+e.Data));
            }

            return;
        }
        /// <summary>
        /// 获得URL地址的文件名称部分
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string getNameFromURL(string url)
        {//获得URL地址中的文件名称部分。第一个问号以前、问号以前的第一个/符号之后部分
            //例如：http://www.lieshu.net/index.htm?fang=good
            try
            {
                int iIndexQ = url.IndexOf("?");
                if (iIndexQ > 0)//如果存在问号
                    url = url.Substring(0, iIndexQ);
                url=url.Substring(url.LastIndexOf("/")+1,url.Length-url.LastIndexOf("/")-1);
                return url;
            }
            catch(Exception ex){
                string info = ex.Message + ex.Data;
                //  MessageBox.Show("getNameFromURL"+ex.Message+ex.Data);
                return null;
            }

        }
        /// <summary>
        /// 获得主机名部分 http://www.lieshu.net/
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string getHostFromURL(string url)
        {

            try
            {
                url = url.Substring(0, url.IndexOf("/", 8) + 1);
                return url;
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                // MessageBox.Show("getHostFromURL"+ex.Message+ex.Data);
                return null;
            }
        }
        /// <summary>
        /// 获得网页的头部，如 http://www.lieshu.net/fang/ok.htm 的http://www.lieshu.net/fang/
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string getPathFromURL(string url)
        {
            try
            {
                url = url.Substring(0, url.LastIndexOf("/") + 1);
                return url;
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
                //MessageBox.Show("getPathFromURL"+ex.Message+ex.Data);
                return null;
            }
        }
        /// <summary>
        /// 转化成存储格式，把最后一个.号改成下划线，再加上htm
        /// 比如 fang.asp转化为fang_asp.htm,便于回访
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string changeToSaveFormat(string url)
        {
            if (url == "")
                url = ".";//如果为空，就给他一个点号
            return url = url.Replace(".", "_") + ".htm";   
        }
        /// <summary>
        /// 转化为访问格式，即把最后一个下划线改成点号，去掉.htm
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string changeToVisitFormat(string url)
        {
            
            string urlhead;
            string urlfoot;

            if (url.IndexOf("_") < 0)//如果没有下划线，直接返回
               return url;
          
            url = url.Substring(0, url.LastIndexOf("."));
            urlhead=url.Substring(0, url.LastIndexOf("_"));
            urlfoot=url.Substring(url.LastIndexOf("_")+1,url.Length-url.LastIndexOf("_")-1);
            url = urlhead + "." + urlfoot;
            if (url == ".")
                url = "";
            return url;
        }
    }
}