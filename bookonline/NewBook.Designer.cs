﻿namespace BookOnline
{
    partial class addform
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBookName = new System.Windows.Forms.Label();
            this.txBookName = new System.Windows.Forms.TextBox();
            this.btOk = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.lbBookURL = new System.Windows.Forms.Label();
            this.cbBookType = new System.Windows.Forms.ComboBox();
            this.lbBookType = new System.Windows.Forms.Label();
            this.txBookURL = new System.Windows.Forms.TextBox();
            this.lbBookInfo = new System.Windows.Forms.Label();
            this.txBookInfo = new System.Windows.Forms.TextBox();
            this.lbBookWriter = new System.Windows.Forms.Label();
            this.txBookWriter = new System.Windows.Forms.TextBox();
            this.btAdvance = new System.Windows.Forms.Button();
            this.cbMany = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lbBookName
            // 
            this.lbBookName.AutoSize = true;
            this.lbBookName.Location = new System.Drawing.Point(31, 92);
            this.lbBookName.Name = "lbBookName";
            this.lbBookName.Size = new System.Drawing.Size(71, 12);
            this.lbBookName.TabIndex = 9;
            this.lbBookName.Text = "书名(必填):";
            // 
            // txBookName
            // 
            this.txBookName.Location = new System.Drawing.Point(108, 87);
            this.txBookName.Name = "txBookName";
            this.txBookName.Size = new System.Drawing.Size(129, 21);
            this.txBookName.TabIndex = 2;
            this.txBookName.Leave += new System.EventHandler(this.txBookName_Leave);
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(45, 393);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(75, 23);
            this.btOk.TabIndex = 5;
            this.btOk.Text = "确定";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.button1_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(153, 393);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 6;
            this.btCancel.Text = "取消";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // lbBookURL
            // 
            this.lbBookURL.AutoSize = true;
            this.lbBookURL.Location = new System.Drawing.Point(31, 194);
            this.lbBookURL.Name = "lbBookURL";
            this.lbBookURL.Size = new System.Drawing.Size(71, 12);
            this.lbBookURL.TabIndex = 9;
            this.lbBookURL.Text = "目录(必填):";
            // 
            // cbBookType
            // 
            this.cbBookType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBookType.FormattingEnabled = true;
            this.cbBookType.ItemHeight = 12;
            this.cbBookType.Location = new System.Drawing.Point(108, 38);
            this.cbBookType.Name = "cbBookType";
            this.cbBookType.Size = new System.Drawing.Size(73, 20);
            this.cbBookType.TabIndex = 1;
            // 
            // lbBookType
            // 
            this.lbBookType.AutoSize = true;
            this.lbBookType.Location = new System.Drawing.Point(31, 41);
            this.lbBookType.Name = "lbBookType";
            this.lbBookType.Size = new System.Drawing.Size(71, 12);
            this.lbBookType.TabIndex = 9;
            this.lbBookType.Text = "类别(必填):";
            // 
            // txBookURL
            // 
            this.txBookURL.Location = new System.Drawing.Point(108, 187);
            this.txBookURL.Name = "txBookURL";
            this.txBookURL.Size = new System.Drawing.Size(233, 21);
            this.txBookURL.TabIndex = 4;
            this.txBookURL.Leave += new System.EventHandler(this.txBookURL_Leave);
            // 
            // lbBookInfo
            // 
            this.lbBookInfo.AutoSize = true;
            this.lbBookInfo.Location = new System.Drawing.Point(31, 246);
            this.lbBookInfo.Name = "lbBookInfo";
            this.lbBookInfo.Size = new System.Drawing.Size(71, 12);
            this.lbBookInfo.TabIndex = 9;
            this.lbBookInfo.Text = "介绍(选填):";
            // 
            // txBookInfo
            // 
            this.txBookInfo.Location = new System.Drawing.Point(108, 243);
            this.txBookInfo.Multiline = true;
            this.txBookInfo.Name = "txBookInfo";
            this.txBookInfo.Size = new System.Drawing.Size(233, 119);
            this.txBookInfo.TabIndex = 5;
            // 
            // lbBookWriter
            // 
            this.lbBookWriter.AutoSize = true;
            this.lbBookWriter.Location = new System.Drawing.Point(31, 143);
            this.lbBookWriter.Name = "lbBookWriter";
            this.lbBookWriter.Size = new System.Drawing.Size(71, 12);
            this.lbBookWriter.TabIndex = 9;
            this.lbBookWriter.Text = "作者(选填):";
            // 
            // txBookWriter
            // 
            this.txBookWriter.Location = new System.Drawing.Point(108, 137);
            this.txBookWriter.Name = "txBookWriter";
            this.txBookWriter.Size = new System.Drawing.Size(129, 21);
            this.txBookWriter.TabIndex = 3;
            // 
            // btAdvance
            // 
            this.btAdvance.Location = new System.Drawing.Point(261, 393);
            this.btAdvance.Name = "btAdvance";
            this.btAdvance.Size = new System.Drawing.Size(75, 23);
            this.btAdvance.TabIndex = 12;
            this.btAdvance.Text = "<<高级";
            this.btAdvance.UseVisualStyleBackColor = true;
            this.btAdvance.Click += new System.EventHandler(this.btAdvance_Click);
            // 
            // cbMany
            // 
            this.cbMany.AutoSize = true;
            this.cbMany.Location = new System.Drawing.Point(258, 42);
            this.cbMany.Name = "cbMany";
            this.cbMany.Size = new System.Drawing.Size(72, 16);
            this.cbMany.TabIndex = 13;
            this.cbMany.Text = "连续添加";
            this.cbMany.UseVisualStyleBackColor = true;
            // 
            // addform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 442);
            this.Controls.Add(this.cbMany);
            this.Controls.Add(this.btAdvance);
            this.Controls.Add(this.txBookWriter);
            this.Controls.Add(this.lbBookWriter);
            this.Controls.Add(this.txBookInfo);
            this.Controls.Add(this.lbBookInfo);
            this.Controls.Add(this.txBookURL);
            this.Controls.Add(this.lbBookType);
            this.Controls.Add(this.cbBookType);
            this.Controls.Add(this.lbBookURL);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.txBookName);
            this.Controls.Add(this.lbBookName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "addform";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "增加新书籍";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbBookName;
        private System.Windows.Forms.TextBox txBookName;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Label lbBookURL;
        private System.Windows.Forms.Label lbBookType;
        private System.Windows.Forms.TextBox txBookURL;
        private System.Windows.Forms.Label lbBookInfo;
        private System.Windows.Forms.TextBox txBookInfo;
        private System.Windows.Forms.Label lbBookWriter;
        private System.Windows.Forms.TextBox txBookWriter;
        private System.Windows.Forms.ComboBox cbBookType;
        private System.Windows.Forms.Button btAdvance;
        private System.Windows.Forms.CheckBox cbMany;
    }
}